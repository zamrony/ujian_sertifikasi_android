package com.example.sertifikasi_android.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiConfig {
    public static  ApiService getApiService(){
        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("http://10.10.2.225/apimobile/")
                .baseUrl("http://192.168.43.63/apimobile/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);
        return service;
    }
}
