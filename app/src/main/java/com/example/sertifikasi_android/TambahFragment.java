package com.example.sertifikasi_android;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.sertifikasi_android.api.ApiConfig;
import com.example.sertifikasi_android.api.ApiService;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class TambahFragment extends Fragment {


    private ImageView iconItemName;
    private EditText edtNamaBarang;
    private ImageView iconPriceTag;
    private EditText edtHargaBarang;
    private ImageView iconMinus;
    private ImageView iconPlus;
    private EditText edtStokBarang;
    private EditText deskripsiBarang;
    private ImageView ivPhoto;
    private EditText edtUrlGambar;
    private Button btnCheckUrl;
    private Button btnKirim;

    private int stokBarang = 0;

    public TambahFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_tambah, container, false);
        initView(view);

        edtStokBarang.setText(""+stokBarang);
//        iconPlus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (edtStokBarang.getText().toString().isEmpty()){
//                    resetStokBarang();
//                    tambahStokBarang();
//                } else {
//                    tambahStokBarang();
//                }
//            }
//        });

        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApiService apiService = ApiConfig.getApiService();
                apiService.tambahData(edtNamaBarang.getText().toString().trim(),
                        edtUrlGambar.getText().toString().trim(),
                        deskripsiBarang.getText().toString().trim(),
                        edtHargaBarang.getText().toString().trim(),
                        edtStokBarang.getText().toString().trim())
                        .enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()){
                                    Toast.makeText(getActivity(), "Sukses Tambah",
                                            Toast.LENGTH_SHORT).show();
                                    edtNamaBarang.setText("");
                                    edtUrlGambar.setText("");
                                    deskripsiBarang.setText("");
                                    edtHargaBarang.setText("");
                                    edtStokBarang.setText("");
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });

        return view;
    }

    private void initView(View view) {
        iconItemName = view.findViewById(R.id.icon_item_name);
        edtNamaBarang = view.findViewById(R.id.edt_nama_barang);
        iconPriceTag = view.findViewById(R.id.icon_price_tag);
        edtHargaBarang = view.findViewById(R.id.edt_harga_barang);
        iconMinus = view.findViewById(R.id.icon_minus);
        iconPlus = view.findViewById(R.id.icon_plus);
        edtStokBarang = view.findViewById(R.id.edt_stok_barang);
        deskripsiBarang = view.findViewById(R.id.deskripsi_barang);
        ivPhoto = view.findViewById(R.id.iv_photo);
        edtUrlGambar = view.findViewById(R.id.edt_url_gambar);
        btnCheckUrl = view.findViewById(R.id.btn_check_url);
        btnKirim = view.findViewById(R.id.btn_kirim);
    }
}
